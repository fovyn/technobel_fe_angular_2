import { Directive, ElementRef, AfterViewInit, Input, Output, EventEmitter, ContentChildren, QueryList } from '@angular/core';
import { MCarouselImgDirective } from './m-carousel-img.directive';

@Directive({
  selector: '[mCarousel]'
})
export class MCarouselDirective implements AfterViewInit {
  //Argument d'entré dans la directive => [mCarousel]=""
  @Input("mCarousel") options: Partial<M.CarouselOptions> = {};
  //Event de sortie de la directive => (mInstance)="XXX"
  @Output("mInstance") mInstanceChange = new EventEmitter<M.Carousel>();
  //Arugment de sortie de la directive => (mCarouselChange)="" mais comme on a un input nommé mCarousel on peut écrire [(mCarousel)]=""
  // @Output("mCarouselChange") mCarouselChange = new EventEmitter<M.Carousel>();
  instance: M.Carousel = null;

  @ContentChildren(MCarouselImgDirective) imgs: QueryList<Element> = new QueryList();

  constructor(private elRef: ElementRef<HTMLDivElement>) { }

  ngAfterViewInit() {
    // {...this.options} => permet l'ajout d'options par défaut avant le ...
    const instance = M.Carousel.init(this.Element, {duration: 500, ...this.options});
    //Lancement de l'évenement avec comme valeur une instance M.Carousel => voir new EventEmitter<M.Carousel>()
    this.mInstanceChange.emit(instance);
    this.instance = instance;

    console.log(this.imgs);
  }

  get Element(): Element {
    return this.elRef.nativeElement;
  }
}
