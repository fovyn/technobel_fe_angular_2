import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[mCarouselImg]'
})
export class MCarouselImgDirective {

  constructor(private elRef: ElementRef<HTMLImageElement>) { }

  get Image(): HTMLImageElement {
    return this.elRef.nativeElement;
  }
}
