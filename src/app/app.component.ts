import { Component, AfterViewInit, ViewChild, ContentChild, OnInit, ElementRef } from '@angular/core';
import { MCarouselDirective } from './directives/m-carousel.directive';
import { MCollapsibleDirective } from './directives/m-collapsible.directive';
import { Produit } from './models';
import { StateType } from './models/enums';
import { CategoryType } from './models/enums/category-type.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild(MCarouselDirective, {static: false}) 
  mCarousel: MCarouselDirective;

  color: string;


  ngOnInit(): void {
    
  }

  ngAfterViewInit() {
    console.log(this.mCarousel);
  }
  
}
