import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Produit } from 'src/app/models';
import { CategoryType, StateType } from 'src/app/models/enums';
import { ProductExistsError } from '../product-exists.error';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private _products: BehaviorSubject<Produit[]> = new BehaviorSubject([]);
  constructor(private httpClient: HttpClient) {
   
  }

 /**
  * @description Method to get all product
  * @return Observable<Produit[]>
  */
 getProducts(): Observable<Produit[]> {
   this.httpClient.get<Produit[]>("http://localhost:3000/api/products").subscribe(data => this._products.next(data));

   return this._products;
 }

 getProduct(id: number): Observable<Produit> {
   return this.httpClient.get<Produit>(`http://localhost:3000/api/products/${id}`)
 }

 addProduct(product: Produit) {
   this._products.next([...this._products.value, product]);

   this.httpClient.post("http://localhost:3000/api/products", product).subscribe(data => console.log(data));
 }

 get List(): Observable<Produit[]> {
   return this._products.asObservable();
 }




}
