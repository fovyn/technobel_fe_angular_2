import { ProductService } from './../../services/product.service';
import { Component, OnInit, AfterContentChecked, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CategoryType } from 'src/app/models';
import { PRODUIT_FORM_CREATE, sameAsValidator } from '../../forms/produit.form';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss']
})
export class ProductCreateComponent implements OnInit, AfterContentChecked, AfterViewInit {
  createForm: FormGroup;
  categories = CategoryType;


  constructor(private formBuilder: FormBuilder, private productService: ProductService) { }
  ngAfterContentChecked(): void {
  }

  ngAfterViewInit() {
  
  }

  ngOnInit(): void {
    this.createForm = this.formBuilder.group(PRODUIT_FORM_CREATE, {validators: [sameAsValidator('title', 'shortDescription')]});
  }
  onSubmitAction() {
    console.log(this.createForm.value);
    const produit = this.createForm.value;

    produit["categoryId"] = this.categories[produit.category];
    delete produit.category;

    if (this.createForm.valid) {
      console.log("IS_VALID");
      this.productService.addProduct(produit);
    } else {
      console.log("IS_INVALID");
    }
  }


  get Categories(): Array<number> {
    const a = [];

    for(let i in this.categories) {
      const index = i as any;
      if (isNaN(index))continue;
      a.push(index);
    }

    return a;
  }
}
