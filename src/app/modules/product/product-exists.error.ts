import { Produit } from 'src/app/models';

export class ProductExistsError extends Error {
    product: Produit;

    constructor(product: Produit) {
        super(`Le produit ${product.id} existe déjà`);
        this.product = product;
    }

}