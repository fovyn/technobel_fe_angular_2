import { AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators, FormGroup } from '@angular/forms';

export function emailValidator(pattern: RegExp): ValidatorFn {
    return (fc: FormControl) => {
        const value = fc.value as string;

        return value != null && value.match(pattern)? null : {emailValidator: "Cannot match pattern"}
    }
}

export function sameAsValidator(fc1Name: string, fc2Name: string): ValidatorFn{
    return (fg: FormGroup) => {
        const fc1 = fg.get(fc1Name);
        const fc2 = fg.get(fc2Name);

        if (!(fc1 && fc2)) return {sameAs: `Not field found for ${fc1Name} OR ${fc2Name}` };

        const fc1Value = fc1.value;
        const fc2Value = fc2.value;

        return fc1Value == fc2Value ? null : { sameAs: 'Values are not the same'}
    }
}

//Déclare un tableau dont la clé est de type string et le type de valeur de la clé est de type AbstractControl
export type FormType = {[key: string]: AbstractControl};

export const PRODUIT_FORM_CREATE: FormType = {
    title: new FormControl(null, [Validators.required, emailValidator(/.*\..*@bstorm\.be/)]),
    shortDescription: new FormControl(null, [Validators.required]),
    images: new FormControl(null, []),
    price: new FormControl(null, [Validators.min(0)]),
    category: new FormControl(null, [])
}