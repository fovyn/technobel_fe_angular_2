import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';
import { CountryModel } from '../models/country.model';

@Injectable()
export class CountryResolver implements Resolve<CountryModel[]> {
  
  countryUrl:string = 'https://restcountries.eu/rest/v2/all';

  constructor(
    private http: HttpClient
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): CountryModel[] | Observable<CountryModel[]> | Promise<CountryModel[]> {
    return this.http.get<CountryModel[]>(this.countryUrl);
  }
}
