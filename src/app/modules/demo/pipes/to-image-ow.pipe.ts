import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toImageOW'
})
export class ToImageOWPipe implements PipeTransform {

  /**
   * Methode permettant de transormer l'image en url
   * @param value string != "" && value != null
   * @param imageSize number >= 0 default 0
   * @throws Error tq value == null || value == "" || size < 0
   * @return string
   */
  transform(imageName: string, imageSize: number = 0): string {
    if (imageName == null || imageName == "") {
      throw new Error("Value cannot be null or empty")
    }
    if (imageSize < 0) {
      throw new Error("Size must be greater or equals than 0")
    }
    return 'http://openweathermap.org/img/wn/' + imageName + '@' + imageSize + 'x.png';
  }

}
