import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MeteoModel } from '../../models/meteo.model';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  apiKey: string = 'd52e50e34214ff0b92247f788638eeb9';

  url: string = 'http://api.openweathermap.org/data/2.5/weather';

  city:string = 'nivelles';

  model: MeteoModel;

  constructor (
    /* injection de dependance  */
    /* private nomDeLaVariable : TypeDuService */
    private http : HttpClient
  ) { }

  ngOnInit(): void {
    let params = new HttpParams();
    params = params.append('appid', this.apiKey);
    params = params.append('q', this.city);
    params = params.append('units', 'metric');
    this.http.get<MeteoModel>(this.url, { params }).subscribe(
      // la methode qui sera exécuter après l'obtention des données de l'api
      data => {
        this.model = data
      },
      // en cas d'erreur
      error => {},
      // dans tous les cas
      () => {}
    );

    // let promise = new Promise(() =>{});
    // promise.then(() => {});
    // promise.catch(() => {});
    // promise.finally(() =>{});
  }

}
